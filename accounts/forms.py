from django import forms
from django.utils.translation import ugettext as _


class SignupForm(forms.Form):
    """
    This form creates additional fields(first_name, last_name) asking from user while signup process
    """
    first_name = forms.CharField(max_length=30, required=True, label=_('Prenom'))
    last_name = forms.CharField(max_length=30, required=True, label=_('Nom'))

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
