# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfile(models.Model):
    """
    Extends builtin User model
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # Add new custom fields for User model below
    dummy_field = models.CharField(max_length=20, blank=True)

    class Meta:
        verbose_name = _('user profile')
        verbose_name_plural = _('user profiles')

    def __unicode__(self):
        return u"%s" % self.user.first_name

    def __str__(self):
        return self.user.first_name


# Next we have to do some magic -
# when the new User creates or updates existing we need to create or update UserProfile too.
@receiver(post_save, sender=User)
def create_or_save_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()
