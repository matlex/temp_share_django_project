# Lingerie Click & Try Service #
## What is this? ##
Web application based on Django.
## What it does? ##
Registers users, produces click & try lingerie web-service for customers before buying.
## How to run? ##
1. Create a clean virtual environment.

~~Install MySQLdb Client to a system. Run: sudo apt-get install mysql-server~~
~~Run: sudo apt-get install python-dev libmysqlclient-dev~~

4. Run: sudo apt-get install build-essential libssl-dev libffi-dev python-dev
5. Go to main application's directory and run pip install -r requirements.txt
6. Generate a new secret key for Django project - http://www.miniwebtool.com/django-secret-key-generator/
7. Copy template_local_settings_and_secrets.json to local_settings_and_secrets.json and fill in ALL setting credentials:
8. Now we need create new database migrations. Run: python manage.py makemigrations
9. You need to create migrations for every app (list of apps you'll find below). At the moment run: python manage.py makemigrations accounts lingerie_app payments
10. Then apply newly created migrations: python manage.py migrate
11. Create a Django admin by running command: python manage.py createsuperuser
12. Launch application by running command "python manage.py runserver 0.0.0.0:8000"
13. Enter to Django admin panel http://your_server_address:8000/admin/ and change default 'example.com' site in 'Sites' section to current domain. See picture - http://joxi.ru/QY2LY7ZFNRzGA6
14. At the 'SOCIAL ACCOUNTS' section enter to 'Social Applications' subsection and fill in data as described on picture - http://joxi.ru/lbrR5MXfb0DEA1
15. Open http://your_server_address:8000 in your browser. All should work.

# Big Updates !!! #
## We moved from mysql to postgresql >>> ##
### So please run following on your local machines: ###

1. sudo apt-get install libpq-dev postgresql postgresql-contrib
2. sudo su - postgres
3. psql
4. create database dev_lingerie;
5. create user djangouser with password 'vigukihejo';
6. grant all privileges on database dev_lingerie to djangouser;
7. \q
8. exit
9. Add following string to your local_settings_json file: "DB_ENGINE": "django.db.backends.postgresql_psycopg2",
10. workon lingerie_venv
11. cd to main dev project directory
12. run: pip install -r requirements.txt
13. run: python manage.py makemigrations
14. run: python manage.py migrate
15. run: python manage.py createsuperuser
16. Add superuser credentials and create it
17. Run localserver server
18. All should work

## List of project apps ##
* accounts
* lingerie_app
* payments
* profiles
* products_catalog

## Prerequisites ##
~~You should have installed a mysql-server on your machine. [For info look here.](http://zetcode.com/databases/mysqltutorial/installation/)~~
~~You should have created a fresh MySQL Database on your machine.~~

## Database Tips ##
~~Creating MySQL DB: CREATE DATABASE lingerie_service CHARACTER SET utf8;~~

## PIP & Virtualenv Tips ##
* If you have no PIP installed on your system (check with 'pip freeze' command) then run: sudo apt install python-pip
* Now update pip: pip install --upgrade pip
* Then you need to install virtualenv: sudo pip install virtualenv
* Also you need install virtualenvwrapper: sudo pip install virtualenvwrapper
* All packages should be installed into activated virtual environment without sudo: eg. pip install requests

### Configuring Virtualenvwrapper with Shell Startup File (.bashrc) ###
* Create a directory where will be stored all your virtualenvs: mkdir ~/.virtualenvs
* Then configure environment variables as described at http://virtualenvwrapper.readthedocs.io/en/latest/install.html#shell-startup-file
* (Optional) If something goes wrong - you may install virtualenvwrapper when you at virtualenv: pip install virtualenvwrapper

### Installing MySQLdb Client ###
~~* As stated [here](https://github.com/PyMySQL/mysqlclient-python) run: sudo apt-get install python-dev libmysqlclient-dev~~

## ToDos:
Important to create client charge amount in django admin !