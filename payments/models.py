# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
from django.dispatch import receiver

from products_catalog.models import Product


class CreditCard(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    card_id = models.CharField(max_length=256, null=False)
    card_exp_month = models.CharField(max_length=2, null=False)
    card_exp_year = models.CharField(max_length=4, null=False)

    def __unicode__(self):
        return unicode(self.card_id)

    def __str__(self):
        return str(self.card_id)


class Payment(models.Model):
    user = models.ForeignKey(User)
    credit_card = models.ForeignKey(CreditCard, null=True)
    processing_start_date = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField(default=0)
    currency = models.CharField(max_length=5, null=False, default='EUR')
    payment_id = models.CharField(max_length=256)
    payment_state = models.CharField(max_length=256)
    payment_date = models.DateTimeField(null=True, auto_now_add=True)
    # Status
    STATUS = (
        (0, _('New')),
        (1, _('Sent for processing')),
        (2, _('Payed')),
        (3, _('Error'))
    )
    # 0 - new
    # 1 - sent for processing
    # 2 - payed
    # 3 - error
    status = models.PositiveIntegerField(choices=STATUS, default=0, blank=False)
    TYPES = (
        (0, _('Charge')),
        (1, _('Refund'))
    )
    type = models.PositiveIntegerField(choices=TYPES, default=0, blank=False)


class ClientChargeAmount(models.Model):
    amount_to_charge = models.IntegerField(verbose_name=_('client charge amount'), unique=True)

    def __unicode__(self):
        return unicode(self.amount_to_charge)

    def __str__(self):
        return str(self.amount_to_charge)


class Basket(models.Model):
    payment = models.ForeignKey(Payment, related_name='payment')
    STATUS = (
        (0, _('shopping	time')),
    )
    status = models.PositiveIntegerField(choices=STATUS, default=0, blank=False)
    products = models.ManyToManyField(Product, blank=True)

    def __unicode__(self):
        return unicode('Basket for {} {}'.format(self.payment.user.first_name, self.payment.user.last_name))

    def __str__(self):
        return str('Basket for {} {}'.format(self.payment.user.first_name, self.payment.user.last_name))


@receiver(post_save, sender=Payment)
def create_user_basket(sender, instance, **kwargs):
    Basket.objects.get_or_create(
        payment=instance,
        status=0
    )
