"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='payments_index'),
    url(r'^payment-return/$', views.payments_return, name='payments_return'),
    url(r'^payment-cancel/$', views.payments_cancel, name='payments_cancel'),
    url(r'^payment-notifications/$', views.payments_notification, name='payments_notification'),
    url(r'^payment-admin/$', views.payments_admin, name='payments_admin')
]

