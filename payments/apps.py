from __future__ import unicode_literals

from django.apps import AppConfig


class PaymentsAppConfig(AppConfig):
    name = 'payments'
