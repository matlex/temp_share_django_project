from django.contrib import admin

# Register your models here.
from .models import Payment, ClientChargeAmount, Basket


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    fields = ('user', 'credit_card')
    list_display = ('user', 'credit_card', 'processing_start_date', 'amount', 'currency', 'payment_id', 'payment_state', 'payment_date', 'status', 'type')

    def has_add_permission(self, request):
        return False


@admin.register(Basket)
class BasketAdmin(admin.ModelAdmin):
    list_display = ['get_basket_username', 'status']

    def get_basket_username(self, obj):
        return 'Basket for: {} {}'.format(obj.payment.user.first_name, obj.payment.user.last_name)

admin.site.register(ClientChargeAmount)
