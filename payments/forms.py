from django.utils.translation import ugettext_lazy as _
from django import forms
from django.contrib.auth.models import User


class AdminPaymentForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all().only('id', 'first_name'))
    amount = forms.FloatField(label='Amount')


class RefundForm(forms.Form):
    form_payment_id = forms.IntegerField(widget=forms.HiddenInput)
    form_amount = forms.FloatField(widget=forms.HiddenInput)


class PaymentCreditCardForm(forms.Form):
    cardholder_name = forms.CharField(max_length=50, required=True, label=_(u"Cardholder name"))
    card_number = forms.CharField(max_length=20, required=True, label=_(u"Card number"))
    card_expiration_date = forms.DateField(input_formats=['%m/%y'], required=True, label=_(u"Credit card expiry date"))
    card_ccv = forms.CharField(max_length=3, required=True, label=_(u"Credit Card Verification"))
    accepted_terms_conditions = forms.BooleanField(
        required=True,
        label=_(u"I accept the general sales conditions *")
    )
