# -*- coding: utf-8 -*-


import payplug
import rollbar

from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from django.core.mail import send_mail
from django.template.loader import render_to_string

from lingerie_project import settings

from .models import CreditCard, Payment, ClientChargeAmount
from .forms import PaymentCreditCardForm, AdminPaymentForm, RefundForm


def paid(request, payment):
    payment_id = str(payment.id)
    payment_state = str(payment.is_paid)
    payment_amount = str(payment.amount)

    local_payment_id = payment.metadata['local_payment_id']
    payment_db = Payment.objects.get(id=local_payment_id)

    # print "Card is saved?:", str(resource.save_card)
    # print "Card Id is:", str(resource.card.id)

    if payment.save_card:
        card_id = str(payment.card.id)
        card_exp_month = str(payment.card.exp_month)
        card_exp_year = str(payment.card.exp_year)
        card = CreditCard.objects.get_or_create(user=payment_db.user)[0]
        card.card_id = card_id
        card.card_exp_month = card_exp_month
        card.card_exp_year = card_exp_year
        card.save()
    elif payment.is_paid and not payment.hosted_payment:
        card_id = str(payment.card.id)
        card = CreditCard.objects.get_or_create(user=payment_db.user)[0]
        card.card_id = card_id
        card.save()
    else:
        CreditCard.objects.filter(user=payment_db.user).delete()
        card = None

    payment_db.payment_id = payment_id
    payment_db.payment_amount = payment_amount
    payment_db.payment_state = payment_state
    payment_db.credit_card = card
    payment_db.status = 2
    payment_db.save()


def error_pay(request, payment):
    local_payment_id = payment.metadata['local_payment_id']
    payment_db = Payment.objects.get(id=local_payment_id)
    payment_db.status = 3
    payment_db.save()


@login_required
def index(request):
    payplug.set_secret_key(settings.PAYPLUG_SECRET_KEY)
    client_charge_amount = str(ClientChargeAmount.objects.get())
    if request.POST.get('payplugToken'):
        credit_card_form = PaymentCreditCardForm(request.POST)
        if credit_card_form.is_valid():
            payment_obj = Payment.objects.create(amount=client_charge_amount,
                                                 user=request.user,
                                                 currency='EUR',
                                                 status=0,
                                                 type=0)
            payment_data = {
                'amount': get_amount(client_charge_amount),
                'currency': 'EUR',
                'payment_method': request.POST.get('payplugToken'),
                'customer': {
                    'email': request.user.email,
                    'first_name': request.user.first_name,
                    'last_name': request.user.last_name,
                },
                'save_card': True,
                'metadata': {
                    'local_payment_id': payment_obj.id
                },
            }

            payment = payplug.Payment.create(**payment_data)
            if payment.is_paid:
                paid(request, payment)

                payload = {
                    "customer_name": request.user.first_name,
                }

                html_message_to_send = render_to_string('lingerie_app/email/thanks_after_payments.html', context=payload)
                txt_message_to_send = render_to_string('lingerie_app/email/thanks_after_payments.txt', context=payload)
                try:
                    send_mail(
                        subject=u'Votre commande de Box Lingerie a bien été validée, merci pour votre confiance !',
                        from_email=settings.DEFAULT_FROM_EMAIL,
                        recipient_list=[request.user.email],
                        # recipient_list=[settings.EMAIL_HOST_USER, "genevieve@miroirdemuses.com"],
                        message=txt_message_to_send,
                        html_message=html_message_to_send,
                        fail_silently=False
                    )
                except Exception as err:
                    rollbar.report_message(err.message)

                return render(request, "payments/return.html")
            else:
                error_pay(request, payment)
                return render(request, "payments/cancel.html")
    else:
        credit_card_form = PaymentCreditCardForm()
    
    context = {
        'publishable_key': settings.PAYPLUG_PUBLISH_KEY,
        'form': credit_card_form,
        'payplug_amount': get_amount(client_charge_amount),
        'frontend_amount': client_charge_amount  # This amount for notifying user about his charging. Done in case of.
    }
    return render(request, 'payments/index.html', context)


def payments_return(request):
    return render(request, 'payments/return.html')


def payments_cancel(request):
    return render(request, 'payments/cancel.html')


def refund(request, payment):
    payment_id = str(payment.id)
    payment_state = 2
    payment_amount = str(payment.amount)

    local_payment_id = payment.metadata['local_payment_id']
    payment_db = Payment.objects.get(id=local_payment_id)
    payment_db.payment_id = payment_id
    payment_db.payment_amount = payment_amount
    payment_db.payment_state = payment_state
    payment_db.status = 2
    payment_db.save()


@csrf_exempt
def payments_notification(request):
    request_body = request.body
    try:
        resource = payplug.notifications.treat(request_body)
        rollbar.report_message(request_body, level='debug')
        if resource.object == 'refund':
            refund(request, resource)
        elif resource.is_paid:
            paid(request, resource)
            return HttpResponse({"status": "success"})
    except payplug.exceptions.PayplugError as e:
        rollbar.report_message(e, level='debug')


def get_amount(amount):
    return int(int(amount) * 100)


def is_user_super_user(user):
    if user.is_superuser:
        return True
    else:
        return False


@user_passes_test(is_user_super_user, login_url='/accounts/signup/')
def payments_admin(request):
    context = {}
    if request.POST:
        payplug.set_secret_key(settings.PAYPLUG_SECRET_KEY)
        full_host = request.META['HTTP_HOST']
        payment_obj = Payment.objects.create(amount=str(ClientChargeAmount.objects.get()),
                                             user=request.user,
                                             currency='EUR',
                                             status=0)
        if request.POST.get('charge'):
            payment_obj.type = 0
            # charge
            card = CreditCard.objects.get(user=request.POST['user'])
            payment_data = {
                'amount': get_amount(request.POST.get('amount')),
                'currency': 'EUR',
                'payment_method': card.card_id,
                'customer': {
                    'email': request.user.email,
                    'first_name': request.user.first_name,
                    'last_name': request.user.last_name
                },
                'notification_url': 'http://' + full_host + '/payments/payment-notifications/',
                'metadata': {
                    'local_payment_id': payment_obj.id,
                },
            }
            payplug.Payment.create(**payment_data)
            context['type'] = 'charge'
        elif request.POST.get('refund') == 'true':
            payment_obj.type = 1
            # refund
            payment_obj_paid = Payment.objects.get(pk=request.POST.get('form_payment_id'))
            refund_data = {
                'amount': get_amount(request.POST.get('form_amount')),
                'metadata': {
                    'local_payment_id': payment_obj.id,
                    'form_payment_id': request.POST.get('form_payment_id'),
                    'reason': 'The delivery was delayed',
                },
            }
            payplug_payment = payplug.Refund.create(payment_obj_paid.payment_id, **refund_data)
            payment_obj.status = 3
            context['type'] = 'refund'
        payment_obj.save()
    context['payment_list'] = Payment.objects.filter(type=0)
    context['form'] = AdminPaymentForm()
    context['refund_form'] = RefundForm()
    return render(request, 'payments/admin.html', context)
