from __future__ import unicode_literals

from mptt.models import MPTTModel, TreeForeignKey
import eav

from django.utils.translation import ugettext as _
from django.db import models
from django.contrib.postgres.fields import JSONField


class Category(MPTTModel):
    name = models.CharField(max_length=150, verbose_name=_('Category'))
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    def __unicode__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = _(u'category')
        verbose_name_plural = _(u'categories')


class Product(models.Model):
    category = TreeForeignKey(Category, blank=True, null=True, related_name='category')
    reference_code = models.CharField(max_length=200, verbose_name=_('reference code'))
    purchasing_price = models.PositiveIntegerField(verbose_name=_('purchasing_price'))
    mdm_price = models.PositiveIntegerField(verbose_name=_('mdm_price'))
    cat_budget = models.CharField(max_length=20, verbose_name=_('cat_budget'))

    product_attributes_json = JSONField(blank=True, null=True)

    def __unicode__(self):
        return self.reference_code

    class Meta:
        verbose_name = _(u'product')
        verbose_name_plural = _(u'products')

eav.register(Product)
