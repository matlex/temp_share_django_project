# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-27 17:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products_catalog', '0004_auto_20170127_1723'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productattribute',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='product_attributes', to='products_catalog.Product', verbose_name='product'),
        ),
    ]
