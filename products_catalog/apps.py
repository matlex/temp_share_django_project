from __future__ import unicode_literals

from django.apps import AppConfig


class ProductsCatalogConfig(AppConfig):
    name = 'products_catalog'
    verbose_name = "Products Catalog Application"
