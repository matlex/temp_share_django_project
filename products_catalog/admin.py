from django.contrib import admin
from django.contrib.admin.decorators import register

from .models import Product, Category

from mptt.admin import MPTTModelAdmin, TreeRelatedFieldListFilter
from eav.forms import BaseDynamicEntityForm
from eav.admin import BaseEntityAdmin


@register(Category)
class CategoryAdmin(MPTTModelAdmin):
    list_display = ['name', 'parent']


class ProductAdminForm(BaseDynamicEntityForm):
    class Meta:
        model = Product
        fields = "__all__"


@register(Product)
class ProductAdmin(BaseEntityAdmin):
    form = ProductAdminForm
    list_filter = (
        ('category', TreeRelatedFieldListFilter),
    )
    exclude = ['product_attributes_json']
