from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.DashboardIndex.as_view(), name='dashboard_index'),
    url(r'^products/$', views.DashboardProducts.as_view(), name='dashboard_products'),
    url(r'^baskets/$', views.DashboardBaskets.as_view(), name='dashboard_baskets'),
]
