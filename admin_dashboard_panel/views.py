from payments.models import Payment, Basket
from products_catalog.models import Product

from django.shortcuts import render
from django.views.generic import ListView


class DashboardIndex(ListView):
    template_name = 'admin_dashboard_panel/index.html'
    context_object_name = 'payments_list'
    paginate_by = 10
    queryset = Payment.objects.order_by('-pk')

    # TODO: Add pagination div into index.html template


class DashboardProducts(ListView):
    template_name = 'admin_dashboard_panel/products_list.html'
    context_object_name = 'products_list'
    paginate_by = 10
    queryset = Product.objects.order_by('-pk')

    # TODO: Add pagination div into products_list.html template


class DashboardBaskets(ListView):
    template_name = 'admin_dashboard_panel/baskets_list.html'
    context_object_name = 'baskets_list'
    paginate_by = 10
    queryset = Basket.objects.order_by('-pk')

    # TODO: Add pagination div into baskets_list.html template
