# -*- coding: utf-8 -*-


from django.contrib import admin
from django.contrib.admin.decorators import register
from django.core import urlresolvers

from .models import ClientShippingAddress, ClientBillingAddress


@register(ClientBillingAddress)
class ClientBillingAddressAdmin(admin.ModelAdmin):
    list_display = ['get_username', 'billing_address_line1', 'billing_address_line2', 'billing_postal_code', 'billing_city']

    def get_username(self, obj):
        return u"{} {}".format(obj.customer.first_name, obj.customer.last_name)


@register(ClientShippingAddress)
class ClientShippingAddressAdmin(admin.ModelAdmin):
    list_display = ['name', 'firstname', 'address_line1', 'address_line2', 'deliver_to', 'postal_code',
                    'city', 'telephone', 'billing_address_is_same', 'get_client_profile_url',
                    'get_client_billing_address'
                    ]

    # This magic is for representing a ForeignKey field in ClientShippingAddress table/list records.
    def get_client_profile_url(self, obj):
        try:
            link = urlresolvers.reverse("admin:auth_user_change", args=[obj.customer.id])
            return u'<a target="_blank" href="{0}">{1}</a>'.format(link, obj.customer.first_name)
        except AttributeError:
            return obj.customer

    # Show link to Billing address form
    def get_client_billing_address(self, obj):
        #  Example: http://192.168.1.35:8000/admin/lingerie_app/clientbillingaddress/1/change/
        billing_address_obj = ClientBillingAddress.objects.get(customer=obj.customer.id)
        link = urlresolvers.reverse("admin:lingerie_app_clientbillingaddress_change", args=[billing_address_obj.id])
        return u'<a target="_blank" href="{0}">{1}</a>'.format(link, "Open billing address")

    get_client_profile_url.allow_tags = True
    get_client_profile_url.short_description = "customer profile"
    get_client_profile_url.admin_order_field = "customer profile"

    get_client_billing_address.allow_tags = True
    get_client_billing_address.short_description = "billing address"
