# *-* coding: utf-8 *-*
from __future__ import unicode_literals

from django.apps import AppConfig


class LingerieAppConfig(AppConfig):
    name = 'lingerie_app'
    verbose_name = "lingerie application"
