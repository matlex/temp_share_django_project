# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.db import models
from django.contrib.auth.models import User


class ClientShippingAddress(models.Model):
    """
    Stores shipping addresses which registered users entered on /shipping web page.
    """
    DELIVER_TO = (
        (0, _('Home')),
        (1, _('Office')),
    )

    customer = models.OneToOneField(User, unique=True, null=False)
    name = models.CharField(max_length=150, verbose_name=_('name'))
    firstname = models.CharField(max_length=150, verbose_name=_('first name'))
    address_line1 = models.CharField(max_length=255, verbose_name=_('address line 1'))
    address_line2 = models.CharField(max_length=255, blank=True, verbose_name=_('address line 2'))
    deliver_to = models.PositiveIntegerField(choices=DELIVER_TO, verbose_name=_('deliver to'))
    postal_code = models.CharField(max_length=150, verbose_name=_('postal code'))
    city = models.CharField(max_length=50, verbose_name=_('city'))
    telephone = models.CharField(max_length=20, verbose_name=_('telephone'))
    billing_address_is_same = models.BooleanField(default=True, verbose_name=_('billing address is same'))

    class Meta:
        verbose_name = _(u'client shipping address')
        verbose_name_plural = _(u'client shipping addresses')


class ClientBillingAddress(models.Model):
    """
    Stores billing address when registered user entered on shipping web page and fills in shipping address form.
    """
    customer = models.OneToOneField(User, unique=True, null=False)
    billing_address_line1 = models.CharField(max_length=255, blank=True, verbose_name=_('billing_address line 1'))
    billing_address_line2 = models.CharField(max_length=255, blank=True, verbose_name=_('billing_address line 2'))
    billing_postal_code = models.CharField(max_length=50, blank=True, verbose_name=_('billing_postal code'))
    billing_city = models.CharField(max_length=50, blank=True, verbose_name=_('billing_city'))
