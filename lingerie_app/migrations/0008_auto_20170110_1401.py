# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-10 14:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lingerie_app', '0007_clientamounttocharge'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ClientAmountToCharge',
            new_name='ClientChargeAmount',
        ),
    ]
