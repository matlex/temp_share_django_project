from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='lingerie_app_index'),
    url(r'^quiz/$', views.quiz_view, name='quiz_page'),
    url(r'^shipping/$', views.ShippingView.as_view(), name='shipping'),
]
