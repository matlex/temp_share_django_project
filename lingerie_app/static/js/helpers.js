$(document).ready(function () {
    var billing_address_is_same_button = $("#id_billing_address_is_same");
    // Check state when page loads
    if (billing_address_is_same_button.prop("checked") == false) {
        $("div#billing_address_form").show();
    }
    else {
        $("div#billing_address_form").hide().find("input").val("");
    }
    // Check on button clicks
    $(billing_address_is_same_button).click(function () {
        if ($("#id_billing_address_is_same").prop("checked") == false) {
            $("div#billing_address_form").show();
        }
        else {
            $("div#billing_address_form").hide().find("input").val("");
        }
    })
});
