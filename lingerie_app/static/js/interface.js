$(function(){

    $('.mobile-sandvich').on('click', function(){
      $('#mobile-menu').slideToggle(400);
      $('.mobile-sandvich').toggleClass('active');
	  return false;
    });
	$('body').on('click touchstart', function(e){
		if (!$(e.target).closest(".mobile-sandvich, #mobile-menu").length) {
			$('#mobile-menu').slideUp(400);
			$('.mobile-sandvich').removeClass('active');
		}
	});
	
	
	// Adrien - try to collapse mobile menu on click

	$('#mobile-menu ul').on('click', function(){
		$('#mobile-menu').slideUp(400);
		$('.mobile-sandvich').removeClass('active');
	});




	if($('#brands ul').length) {
		$('#brands ul').owlCarousel({
			loop: true,
			autoPlay: 3000,
			margin:10,
			items : 5, //5 items above 1000px browser width
			itemsDesktop : [1199,5],
			itemsDesktopSmall : [979,4],
			itemsTablet: [768,3],
			itemsTabletSmall: [735,3],
			itemsMobile: [479,2],
			navigation : true,
			navigationText : ['',''],
		});
	}
	if($('#press ul').length) {
		$('#press ul').owlCarousel({
			loop: true,
			autoPlay: 3000,
			margin:10,
			items : 5, //5 items above 1000px browser width
			itemsDesktop : [1199,5],
			itemsDesktopSmall : [979,4],
			itemsTablet: [768,3],
			itemsTabletSmall: [735,3],
			itemsMobile: [479,2],
			navigation : true,
			navigationText : ['',''],
		});
	}
	if($('.reviews-slider ul').length) {
		$('.reviews-slider ul').owlCarousel({
			//loop: false,
			margin: 0,
			items : 1,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [979,1],
			itemsTablet: [768,1],
			itemsTabletSmall: [735,1],
			itemsMobile: [479,1],
			navigation : true,
			navigationText : ['',''],
		});
	}
	
});