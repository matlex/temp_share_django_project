from django.utils.translation import ugettext as _
from django import forms
from .models import ClientShippingAddress, ClientBillingAddress


class ClientShippingAddressForm(forms.ModelForm):
    """
    Form for /shipping page
    """
    # Overwrite 'deliver_to' field to make it as RadioSelect instead list select on front page
    deliver_to = forms.TypedChoiceField(
        choices=ClientShippingAddress.DELIVER_TO, widget=forms.RadioSelect, label=''
    )

    class Meta:
        model = ClientShippingAddress
        fields = [
            'deliver_to',
            'name',
            'firstname',
            'address_line1',
            'address_line2',
            'postal_code',
            'city',
            'telephone',
            'billing_address_is_same'
        ]


class ClientBillingAddressForm(forms.ModelForm):
    class Meta:
        model = ClientBillingAddress
        fields = [
            'billing_address_line1',
            'billing_address_line2',
            'billing_postal_code',
            'billing_city',
        ]


class IndexPageContactForm(forms.Form):
    customer_name = forms.CharField(max_length=255, required=True, label='NOM')
    customer_email = forms.EmailField(required=True, label='MAIL')
    customer_text_message = forms.CharField(
        max_length=3000, required=True, widget=forms.Textarea, label='VOTRE MESSAGE'
    )
