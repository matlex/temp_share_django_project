import rollbar

from lingerie_project.settings import DEFAULT_FROM_EMAIL, EMAIL_HOST_USER
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from django.views import View
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.mail import send_mail
from django.template.loader import render_to_string

from .models import ClientShippingAddress, ClientBillingAddress
from .forms import ClientShippingAddressForm, ClientBillingAddressForm, IndexPageContactForm


def index(request):
    if request.POST:
        form = IndexPageContactForm(request.POST)
        if form.is_valid():
            payload = {
                "customer_email": form.cleaned_data.get('customer_email'),
                "customer_name": form.cleaned_data.get('customer_name'),
                "customer_text": form.cleaned_data.get('customer_text_message')
            }
            html_message_to_send = render_to_string('lingerie_app/email/contact_form_email.html', context=payload)
            txt_message_to_send = render_to_string('lingerie_app/email/contact_form_email.txt', context=payload)
            try:
                send_mail(
                    subject='Vous avez une question sur la Home Page',
                    from_email=DEFAULT_FROM_EMAIL,
                    recipient_list=[EMAIL_HOST_USER, "genevieve@miroirdemuses.com"],
                    message=txt_message_to_send,
                    html_message=html_message_to_send,
                    fail_silently=False
                )
            except Exception as err:
                rollbar.report_message(err.message)
            messages.success(request, 'Votre message a bien ete envoye, merci !')
            return HttpResponseRedirect('/')
    else:
        form = IndexPageContactForm()

    context = {
        "form": form,
    }
    return render(request, 'lingerie_app/index.html', context)


@login_required
def quiz_view(request):
    context= {
        "nom_user":request.user,
    }
    
    return render(request=request, template_name='lingerie_app/quiz.html',context=context)


class ShippingView(View):

    @method_decorator(login_required)
    def get(self, request):
        shipping_address_form = ClientShippingAddressForm()
        billing_address_form = ClientBillingAddressForm()
        context = {
            'shipping_address_form': shipping_address_form,
            'billing_address_form': billing_address_form
        }
        return render(request, 'lingerie_app/shipping.html', context)

    @method_decorator(login_required)
    def post(self, request):
        try:
            # Shipping Address Obj & Form
            client_shipping_address_obj = ClientShippingAddress.objects.get(
                customer=User.objects.get(username=request.user)
            )
            shipping_address_form = ClientShippingAddressForm(request.POST, instance=client_shipping_address_obj)
            # Billing Address Obj & Form
            client_billing_address_obj = ClientBillingAddress.objects.get_or_create(
                customer=User.objects.get(username=request.user))[0]
            billing_address_form = ClientBillingAddressForm(request.POST, instance=client_billing_address_obj)

            # Run creating shipping & billing records process
            return self.processing_client_shipping_billing_addresses(
                request=request,
                shipping_address_form=shipping_address_form,
                billing_address_form=billing_address_form
            )

        except ClientShippingAddress.DoesNotExist:
            # Shipping Address Obj & Form
            # If there is no client_shipping_address record in database then extract a current user instance and
            # create a new client_shipping_address record.
            shipping_address_form = ClientShippingAddressForm(request.POST)

            # Billing Address Obj & Form
            client_billing_address_obj = ClientBillingAddress.objects.get_or_create(
                customer=User.objects.get(username=request.user))[0]
            billing_address_form = ClientBillingAddressForm(request.POST, instance=client_billing_address_obj)

            # Run creating shipping & billing records process
            return self.processing_client_shipping_billing_addresses(
                request=request,
                shipping_address_form=shipping_address_form,
                billing_address_form=billing_address_form
            )

    # Helpers
    @staticmethod
    def processing_client_shipping_billing_addresses(request, shipping_address_form, billing_address_form):
        if shipping_address_form.is_valid():
            shipping_address_instance = shipping_address_form.save(commit=False)
            # Applying current user to a instance
            shipping_address_instance.customer = User.objects.get(username=request.user)
            shipping_address_instance.save()
            # If we have billing adress is same as shipping then just save shipping address and duplicate info into
            # billing address
            if shipping_address_form.cleaned_data['billing_address_is_same']:
                client_billing_address_obj = ClientBillingAddress.objects.get_or_create(
                    customer=User.objects.get(username=request.user))[0]
                client_billing_address_obj.billing_address_line1 = shipping_address_instance.address_line1
                client_billing_address_obj.billing_address_line2 = shipping_address_instance.address_line2
                client_billing_address_obj.billing_postal_code = shipping_address_instance.postal_code
                client_billing_address_obj.billing_city = shipping_address_instance.city
                client_billing_address_obj.save()
                return redirect(reverse('payments_index'))

            else:  # Otherwise:
                if billing_address_form.is_valid():
                    if not billing_address_form.cleaned_data["billing_address_line1"]:
                        pass
                    if not billing_address_form.cleaned_data["billing_address_line2"]:
                        pass
                    if not billing_address_form.cleaned_data["billing_postal_code"]:
                        pass
                    if not billing_address_form.cleaned_data["billing_city"]:
                        pass
                    else:
                        billing_address_form.save()
                    return redirect(reverse('payments_index'))
        context = {
            'shipping_address_form': shipping_address_form,
            'billing_address_form': billing_address_form
        }
        return render(request, 'lingerie_app/shipping.html', context)
