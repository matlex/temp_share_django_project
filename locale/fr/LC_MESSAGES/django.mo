��    ,      |      �      �  I   �     '     3     C     J     c     {     �  '   �     �     .     >     U     Y     ]     d     j     v     �     �     �     �     �     �     �  �   �     �     �     �     �     �     �                     1  
   K  
   V     a     f  	   r     |     �  �  �  K         l     }     �     �     �     �     �  /   �  c   �     Y	     k	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  
   
  #   '
  �   K
                '     H     Z     l     �     �     �     �     �     �     �     �     �             Already have an account? Then please <a href="%(login_url)s">sign in</a>. Card number Cardholder name Charge Credit Card Verification Credit card expiry date Error Home I accept the general sales conditions * If you have not created an account yet, then please
                                <a href="%(signup_url)s">sign up</a> first. Important dates Mot de passe oublié ? New Nom Office Payed Permissions Personal info Prenom Refund Secure payment by XX Sent for processing Sign In Signup Verify Your E-mail Address We have sent an e-mail to you for verification. Follow the link provided to finalize the signup process. Please contact us if you do not receive it within a few minutes. address line 1 address line 2 billing address is same billing_address line 1 billing_address line 2 billing_city billing_postal code city client shipping address client shipping addresses deliver to first name name postal code telephone user profile user profiles Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-01 10:44+0000
PO-Revision-Date: 2017-01-01 10:52+0000
Last-Translator:   <mat@ya.ru>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Translated-Using: django-rosetta 0.7.12
 Déjà un compte ? Merci de vous connecter <a href="%(login_url)s">ici</a>. Numéro de carte Nom sur la carte Ordre de paiement CVV Date d'expiration Erreur Accueil J'accepte les conditions générales de vente * Si vous n'avez pas encore créé de compte, merci de vous inscrire
<a href="%(signup_url)s">ixi</a> Dates importantes Mot de passe oublié ? Nouveau Nom Bureau Payé Autorisations Informations personnelles Prénom Remboursement Paiement sécurisé par Payplug Envoi pour traitement Se connecter S'inscrire Merci de vérifier votre boite mail Nous vous avons envoyé un e-mail de vérification. Merci de suivre le lien fourni pour finaliser votre inscription. Si vous rencontrez un problème, n'hésitez pas à nous contacter. Adresse (ligne 1) Adresse (ligne 2) Adresse de facturation identique Adresse (ligne 1) Adresse (ligne 2) Ville de facturation  Code postal de facturation Ville Adresse de livraison Adresse de facturation Lieu de livraison Prénom Nom Code postal Téléphone Profil utilisateur Profils utilisateurs 